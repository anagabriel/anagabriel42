function parseLinks (e, an) {
  let elist = [],
      p = 0,
      bf = -1,
      be = -1,
      pf = -1,
      pe = -1,
      tmp = '',
      name = '',
      href = '';

  const S = {
    BF: 1,
    BE: 2,
    PF: 3,
    PE: 4,
    NONE: 0,
  };

  const reset = () => {
    bf = -1;
    be = -1;
    pf = -1;
    pe = -1;
    name = '';
    href = '';
    state = S.NONE;
  };

  let state = S.NONE;
  while (p < e.length) {
    switch (e[p]) {
      case '[':
        if (state === S.NONE) {
          bf = p;
          state = S.BF;
        } else {
          tmp += e[p];
          reset();
        } break;
      case ']':
        if (state === S.BF && bf < p) {
          be = p;
          state = S.BE;
        } else {
          tmp += '[' + name + e[p];
          reset();
        } break;
      case '(':
        if (state === S.BE && be < p) {
          pf = p;
          state = S.PF;
        } else {
          if (name.length) {
            tmp += '[' + name + ']' + e[p];
          } else tmp += e[p];
          reset();
        } break;
      case ')':
        if (state === S.PF && pf < p) {
          pe = p;
          if (tmp.length) {
            elist.push(tmp);
            tmp = '';
          } elist.push(an(name, href));
        } else {
          if (name.length) {
            tmp += '[' + name + ']' + '(' + href + e[p];
          } else tmp += e[p];
          elist.push(tmp);
          tmp = '';
        } reset();
        break;
      default:
        if (state === S.BE && (e[p] !== ' ' && e[p] !== '\t')) {
          reset();
        } else if (state === S.BF) {
          name += e[p];
        } else if (state === S.PF) {
          href += e[p];
        } else {
          tmp += e[p];
        } break;
    } p++;
  } if (elist.length) {
    elist.push(tmp);
  } return elist;
}

module.exports = {
  parseLinks
}
