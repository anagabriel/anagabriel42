const std = require('./std');
const users = require('../controllers/users');

function admin (req, res, next) {
  const u = {
    id: req.cookies['id'],
    login: req.cookies['login']
  };

  if (u.id != 'ana.gabriel2012@gmail.com') {
    res.json(
      std.response({message: 'Admin needs to login!'})
    );
    return;
  }

  users.check(u.id, u, (err, data) => {
    if (err) {
      res.json(
        std.response(err)
      );
    } return next();
  });
}

function user (req, res, next) {
  const u = {
    id: req.cookies['id'],
    login: req.cookies['login']
  };

  if (u.id != (req.params.id || req.body.id)) {
    res.json(
      std.response({message: `you don't own this account!`})
    );
    return;
  }

  users.check(u.id, u, (err, data) => {
    if (err) {
      res.json(
        std.response(err)
      );
    } return next();
  });
}

module.exports = {admin, user};
