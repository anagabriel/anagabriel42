const dynamodb = require('./DynamoDB');
const docClient = require('./DocClient');
const std = require('../utils/std');

module.exports = {

  set: (table_name, key, limit) => {
    if (!table_name) return false;

    this[table_name] = {
      TableName: table_name,
      Key: key || 'id',
      Limit: limit || -1
    };

    return this[table_name];
  },

  table: (name, callback) => {
    if (!this[name].TableName) return callback(true);

    let schema = [
      { AttributeName: this[name].Key, AttributeType: 'S'}
    ];

    let keys = [
      { AttributeName: this[name].Key, KeyType: 'HASH' }
    ];

    const params = {
      TableName: this[name].TableName,
      KeySchema: keys,
      AttributeDefinitions: schema,
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      }
    };

    dynamodb.createTable(params, (err, data) => {
      return callback(err, data || {});
    });
  },

  all: (name, callback, last) => {
    if (!this[name].TableName) return callback(true);

    let params = {TableName: this[name].TableName};
    if (this[name].Limit !== -1) params.Limit = this[name].Limit;
    if (last) params.ExclusiveStartKey = last;

    docClient.scan(params, (err, data) => {
      return callback(
        err, data ? data.Items : [],
        data ? data.LastEvaluatedKey : null
      );
    });
  },

  count: (name, callback) => {
    if (!this[name].TableName) return callback(true);

    docClient.scan({
      TableName: this[name].TableName,
      Select: 'COUNT'
    }, (err, data) => {
      return callback(
        err,
        data ? data.Count : 0
      );
    });

  },

  get: (name, id, callback) => {
    if (!this[name].TableName) return callback(true);

    let key = {};
    key[this[name].Key] = id;

    const tmp = {
      TableName: this[name].TableName,
      Key: key
    };

    docClient.get(tmp, (err, data) => {
      return callback(err, data ? data.Item : {});
    });
  },

  find: (name, params, callback, last) => {
    if (!this[name].TableName) return callback(true);

    let condition = '';
    let values = {};

    for (let key in params) {
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!std.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      } condition += key + ' = ' + ':' + val + ' and ';
    } condition = condition.substr(0, condition.length-5);

    let tmp = {
      TableName : this[name].TableName,
      FilterExpression: condition,
      ExpressionAttributeValues: values
    }

    if (last) tmp.ExclusiveStartKey = last;

    docClient.scan(tmp, (err, data) => {
      return callback(err, data ? data.Items : [],
                      data ? data.LastEvaluatedKey : null);
    });
  },

  create: (name, params, callback) => {
    if (!this[name].TableName) return callback(true);

    const tmp = {
      TableName: this[name].TableName,
      Item: params
    }

    docClient.put(tmp, (err, data) => {
      return callback(err, data ? data : {});
    });
  },

  update: (name, id, params, callback) => {
    if (!this[name].TableName) return callback(true);

    let UpdateExpression = 'SET ';
    let names = {},
        values = {};

    for (let key in params) {
      if (key === this[name].Key) continue;
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!std.isAlphaNumeric(val[i])) continue;
        if (!values[`:${val[i]}`]) {
          values[`:${val[i]}`] = params[key];
          names[`#${val[i]}`] = key;
          val = val[i];
        }
      } UpdateExpression += `#${val} = :${val} , `;
    } UpdateExpression = UpdateExpression.substr(
      0, UpdateExpression.length - 3
    );

    let Key = {};
    Key[this[name].Key] = id;

    const tmp = {
      Key,
      UpdateExpression,
      ReturnValues: 'ALL_NEW',
      ExpressionAttributeNames: names,
      TableName: this[name].TableName,
      ExpressionAttributeValues: values
    };

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  remove: (name, params, callback) => {
    if (!this[name].TableName) return callback(true);

    let names = {};
    let UpdateExpression = 'REMOVE ';

    for (let key in params) {
      if (key === this[name].Key) continue;
      let k = key[0];
      for (let i = 0; i < key.length; ++i) {
        k = key[i];
        if (!std.isAlphaNumeric(k)) continue;
        if (!names[`#${k}`]) {
          names[`#${k}`] = key;
          i = key.length;
        }
      } UpdateExpression += `#${k} , `;
    } UpdateExpression = UpdateExpression.substr(
      0, UpdateExpression.length - 3
    );

    let key = {};
    key[this[name].Key] = params[this[name].Key];

    const tmp = {
      Key: key,
      ReturnValues: 'ALL_NEW',
      TableName: this[name].TableName,
      ExpressionAttributeNames: names,
      UpdateExpression: UpdateExpression
    }

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  delete: (name, id, callback) => {
    if (!this[name].TableName) return callback(true);

    let key = {};
    key[this[name].Key] = id;

    const tmp = {
      TableName: this[name].TableName,
      Key: key
    };

    docClient.delete(tmp, (err, data) => {
      return callback(err, data);
    });
  }

}
