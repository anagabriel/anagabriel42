import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Layout } from 'antd'
import { Nav, Project } from './euler'

class Euler extends Component {
  render () {
    return (
      <Layout>
          <Nav/>
          <Project/>
      </Layout>
    );
  }
}

ReactDOM.render(<Euler/>, document.getElementById('euler'));