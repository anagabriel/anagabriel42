import React, { Component } from 'react'

import { Progress, Layout, Button, message } from 'antd'
const { Content } = Layout

import Submit from './Submit'

import std from '../../../utils/std'
import { grabInfo } from '../../../utils/create'

export default class Single extends Component {

    state = { 
        to_render: (
            <Content className='problem'>loading...</Content>
        ),
    };

    componentDidMount () {
        const { data } = this.props;
        const parse = new DOMParser();

        try {
            const html = parse.parseFromString(data.problem, 'text/html');
            this.setState({
                to_render: this.toReactComponents(html.body, data),
            });
        } catch (err) { 
            console.log(err);
        }
    }

    grabHeaderContent = (data) => {
        const { color, percent } = data;
        
        const header = (<h1>{data.header}</h1>);
        const difficulty = <Progress 
            percent={Number(percent)} 
            status='active'
            showInfo={false}
            strokeColor={color}
        />;

        return {
            header,
            difficulty,
        };
    }

    grabChildren = (elm, list) => {
        if (!elm.childNodes || !elm.childNodes.length) {
            return list;
        }

        for (let e of elm.childNodes) {
            if (e.nodeType >= 3 && e.textContent.trim()) {
                list.push(e.textContent);
            } else if (e.nodeName.toLowerCase() === 'img') {
                let origin = `${window.location.origin}/euler/`;
                let src = `https://projecteuler.net/${e.src.replace(origin, '')}`;
                list.push((<img key={std.string(6)} src={src} />));
            } else if (e.nodeType <= 2 && e.nodeName.toLowerCase() !== 'br') {
                const info = grabInfo(e);
                info[1].key = std.string(12);
                list.push(React.createElement(
                    info[0],
                    info[1],
                    this.grabChildren(e, info[2])
                ));
            } else if (e.nodeName.toLowerCase() === 'br') {
                list.push(<br key={std.string(12)}/>);
            }
        } 
        
        return list;
    }

    toReactElement = (elm) => {
        const info = grabInfo(elm);
        info[1].key = std.string(12);
        return React.createElement(
            info[0], 
            info[1],
            this.grabChildren(elm, info[2]) 
        );
    }

    grabContent = (elms, children) => {
        for (let c of elms) {
            if (c.nodeType <= 2 && c.nodeName.toLowerCase() !== 'br') {
                if (c.nodeName.toLowerCase().indexOf('img') > -1) {
                    let origin = `${window.location.origin}/euler/`;
                    let src = `https://projecteuler.net/${c.src.replace(origin, '')}`;
                    children.push((<img key={std.string(6)} src={src} />));
                } else children.push(this.toReactElement(c));
            } else if (c.nodeType === 3 && c.textContent.trim()) {
                children.push(c.textContent);
            } else if (c.nodeName.toLowerCase() === 'br') {
                children.push(<br key={std.string(12)}/>);
            }
        }
    }

    onSubmit = (e) => {
        const ta = document.getElementById('submission');
        if (!ta.value.trim()) return;

        const opt = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, body: JSON.stringify({ submit: ta.value.trim() })
        };

        const url = `${window.location.origin}/problems/${this.props.data.id}`;

        fetch(url, opt).
        then(res => res.json()).
        then(res => {
            if (!res.success)
                message.error(
                    res.result.message.toString()
                );
            else
                message.success('Submission was saved!');
        }).catch(() => {});
    }

    toReactComponents = (html, data) => {
        let result;
        if (html && html.children.length) {
            let header, difficulty, content;

            for (let e of html.children) {
                if (e.classList.contains('info')) {
                    const res = this.grabHeaderContent(data);
                    header = res.header;
                    difficulty = res.difficulty;
                } else {
                    const children = [];
                    this.grabContent(e.childNodes, children);
                    content = children;
                }
            }

            result = (
                <Content className='problem'>
                    <aside className='left'>
                        {header}
                        {difficulty}
                        {content}
                    </aside>
                    <aside className={std.isAna() ? 'right ana' : 'right'}>
                        <Submit data={data.submit || 'No submission'}/>
                        {
                            std.isAna() ? 
                            <Button className='submit-button' shape='circle' size='large' icon='save' onClick={this.onSubmit}/> : []
                        }
                    </aside>
                </Content>
            );         
        }

        return result;
    }

    render () {
        setTimeout(images, 500);
        const { to_render } = this.state;
        return to_render;
    }
}