import All from './All';
import Single from './Single';
import Project from './Project';

export { Project, All, Single }