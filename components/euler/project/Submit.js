import React, { Component } from 'react'

import std from '../../../utils/std'

import { Input } from 'antd'
const { TextArea } = Input

class Submit extends Component {

    renderText = (text) => {
        const result = [];

        let txt = '', tabs = 0;
        for (let i = 0; i < text.length; i++) {
            const t = text[i];
            if (txt === '  ' || t === '\t') {
                tabs++;
                txt = '';
            } if (t === '\n') {
                result.push(
                    <p key={std.string(6)}>
                        <span style={{
                            'margin-left': `${tabs * 10}px`
                        }}>{txt}</span>
                    </p>
                );
                txt = '';
                tabs = 0;
            } else {
                txt += t;
            }
        }

        result.push(
            <p key={std.string(6)}>
                <span style={{
                    'margin-left': `${tabs * 10}px`
                }}>{txt}</span>
            </p>
        );

        return <p className='padding'>{result}</p>;
    }

    render () {
        const { data } = this.props;
        return std.isAna() ? 
            <TextArea id='submission' className='submission'>{data}</TextArea>: 
            <div className='code'>{this.renderText(data)}</div>;
    }

}

export default Submit