import React, { Component } from 'react'

import { All, Single } from '.'
import { Layout, Button } from 'antd'

const { Content } = Layout

export default class Project extends Component {
    constructor (props) {
        super(props);
        this.state = {
            page: 0,
            data: [],
            last: [],
            loading: true,
            path: window.location.pathname.split('/'),
        }; 
    }

    componentDidMount () {
        const { path } = this.state;

        let url = `${window.location.origin}/problems`;
        if (path.length > 2 && path[2]) 
            url += `/${path[2]}`;

        fetch(url)
        .then(res => res.json())
        .then(({ success, result }) => {
            this.setState({
                loading: false,
                data: result.data,
                last: result.last ? [result.last.id] : [],
            });
        }).catch(() => {});
    }

    run = (l, p) => {
        const { page } = this.state;
        let url = `${window.location.origin}/problems`;
        if (p) url += `?last=${l[p < page ? p - 1 : page]}`;

        fetch(url)
        .then(res => res.json())
        .then(({ success, result }) => {
            const last = l.slice(); 
            if (p + 1 > last.length) {
                last.push(result.last.id);
            } this.setState({
                last,
                page: p,
                loading: false,
                data: result.data,
            });
        }).catch(() => {});
    }

    onNext = (e) => {
        const { page, last } = this.state;
        if (page >= 6) return;
        this.setState({ loading: true });
        this.run(last, page + 1); 
    }

    onBack = (e) => {
        const { page, last } = this.state;
        if (!page) return;
        this.setState({ loading: true });
        this.run(last, page - 1);   
    }

    render () {
        const { data, total, loading } = this.state;
        let to_render = (<Content className='problem'>loading...</Content>);
        
        if (!loading && data.length > 1)
            to_render = data.map(d => (
                <All key={'problem-'+d.id} data={d}/>
            ));
        else if (!loading && data.length)
            to_render = <Single data={data[0]}/>;

        return loading || data.length < 2 ? to_render : (
            <Content className='problem'>
                <Button type='primary' shape='circle' size='large' onClick={this.onBack} icon='arrow-left' className='left'/>
                <Button type='primary' shape='circle' size='large' onClick={this.onNext} icon='arrow-right' className='right'/> 
                <ul className='problems'>{to_render}</ul>
            </Content>
        );
    }
}