import React, { Component } from 'react'
import { Layout, Menu } from 'antd'

const { Header } = Layout

export default class Nav extends Component {
    render () {
        const all = window.location.pathname.split('/').length <= 2;
        return (
            <Header className="light">
                <Menu
                    theme="light"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                    selectedKeys={all ? ['1'] : []}
                >
                    <Menu.Item key="1"><a href='/euler'>problems</a></Menu.Item>
                </Menu>
            </Header>
        );
    }
}