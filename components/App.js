import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import Content from './content/Content'

import { Provider } from 'react-redux'
import store from '../store'

class App extends Component {

  render () {
    return (
      <div>
        <Content />
      </div>
    );
  }

}

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'));
