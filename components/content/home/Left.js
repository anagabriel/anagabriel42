import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current,
    user: store.user
  };
})

export default class Left extends Component {

  login (e) {
    if (!!this.props.user.id) return;
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: 'login'}
    });
  }

  render () {
    const icon = !!this.props.user.id ? 'user' : 'login';
    return (
      <aside className='left'>
        <div className='top'></div>
        <div className='middle'>
          <ul>
            <li>ana-</li>
            <li>gabriel</li>
            <li>software engineer</li>
          </ul>
        </div>
        <div className='bottom'>
          <Button type='primary' onClick={
            icon === 'login' ?
            e => this.login(e) :
            () => window.location.href='https://dashboard.anagabriel.me'
          } shape='circle' icon={icon}/>
          <div className='bar'></div>
        </div>
      </aside>
    );
  }

}
