import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Divider } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Right extends Component {

  componentDidMount () {
    setTimeout(images, (1000));
  }

  update (e, type) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type}
    });
  }

  render () {
    return (
      <aside className='right'>
        <div className='img'>
          <img data-src='/img/screen.jpg' alt='image of a computer screen'/>
          <span className='toHide'>{'image of a computer screen'}</span>
        </div>
        <div className='bottom'>
          <a onClick={
            e => this.update(e, 'about')
          }>about</a>
          <Divider type='vertical' />
          <a onClick={
            e => this.update(e, 'projects')
          }>projects</a>
          <Divider type='vertical' />
          <a onClick={
            e => this.update(e, 'blog')
          }>blog</a>
        </div>
      </aside>
    );
  }

}
