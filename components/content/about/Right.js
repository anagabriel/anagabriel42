import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as icons from '../icons'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Right extends Component {

  render () {
    return (
      <aside className='right about'>
        <div className='top'>
          <ul>
            <li>about ana-gabriel</li>
            <li>
              I love building web services/websites and other
              softwares, like my programming language, Pumpkin Spice Latte (psl).
              <br/><b>Education:</b> Purdue University -
              Major: Computer Science,
              Minors: Software Engineering, Psychology; Udacity -
              Nanodegree: Mobile Web Specialist
              <br/><b>Skills:</b> JavaScript, HTML, CSS, Node.js, ReactJS,
              NoSQL, SQL, Ruby on Rails, Java, C++, Flex, Bison, PWA,
              Service Worker, Git, AWS, GCP, Heroku, Oracle Database, Hadoop
            </li>
          </ul>
        </div>
        <div className='bottom'>
          <div className='bar'>
            <Button type='primary' shape='circle' href={
              'https://gitlab.com/anagabriel'
            } target='_blank' icon='gitlab'/>
            <Button type='primary' shape='circle' href={
              'https://github.com/anagabriel42'
            } target='_blank' icon='github'/>
            <Button type='primary' shape='circle' href={
              'https://www.linkedin.com/in/anagabrielperez/'
            } target='_blank' icon='linkedin'/>
            <Button type='primary' shape='circle' href={
              'https://devpost.com/Ana-GabrielPerez'
            } target='_blank'><icons.Devpost /></Button>
            <Button type='primary' shape='circle' href={
              'https://stackoverflow.com/users/5556828/ana-gabriel-perez'
            } target='_blank'><icons.StackOverflow /></Button>
          </div>
        </div>
      </aside>
    );
  }

}
