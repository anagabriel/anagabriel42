import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Left extends Component {

  componentDidMount () {
    setTimeout(images, (500));
  }

  home (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  render () {
    return (
      <aside className='left about'>
        <div className='top'>
          <Button shape='circle' icon='arrow-left' onClick={
            e => this.home(e)
          }/>
        </div>
        <div className='middle'>
          <img data-src='img/ana.jpg' alt='image of Ana-Gabriel'/>
        </div>
      </aside>
    );
  }

}
