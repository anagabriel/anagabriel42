import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as blog from './blog'
import * as home from './home'
import * as about from './about'
import * as login from './login'
import * as article from './article'
import * as project from './project'
import * as projects from './projects'

import {
  Button,
  message,
  notification,
} from 'antd'
import 'antd/dist/antd.css'

@connect((store) => {
  return {
    user: store.user,
    current: store.current,
    projects: store.projects
  };
})

export default class Content extends Component {

  componentDidMount () {

    const current = this.props.current;
    const type = current.type.
                         toString().
                         toLowerCase();

    fetch('/cred').
    then(res => res.json()).
    then(res  => {
      if (
        !res.success &&
        document.cookie.indexOf('checked=true') === -1
      ) {
        const key = `open${Date.now()}`;
        notification['info']({
          key,
          duration: 0,
          placement: 'bottomLeft',
          message: 'before continuing...',
          description: `this site uses cookies.`,
          btn: (
            <Button type="primary" size="small" onClick={
              () => {
                document.cookie += ' checked=true;';
                notification.close(key);
              }
            }>sounds good!</Button>
          )
        });
      } if (!res.success) {
        if (res.result.message.toString() !== 'incorrect session!')
          message.error(res.result.message.toString());
        this.props.dispatch({
          type: 'UPDATE_USER',
          payload: {}
        });
        return;
      }

      if (browser().indexOf('edge') !== -1) {
        document.cookie += ` id=${
          res.result.id
        } login=${res.result.login}`;
      }

      this.props.dispatch({
        type: 'UPDATE_USER',
        payload: res.result
      });
    }).catch(
      () => console.log('error on creds')
    );

    fetch('/tabs').
    then(res => res.json()).
    then(res => {
      if (!res.success)
        return console.log(res.result);

      this.props.dispatch({
        type: 'UPDATE_PROJECTS',
        payload: res.result
      });

      if (type === 'project')
        this.props.dispatch({
          type: 'UPDATE_CURRENT',
          payload: {
            type: 'project',
            pro: res.result.find(
              p => p.id == current.pro.id
            )
          }
        });
    }).catch(
      () => console.log('error on tabs')
    );
  }

  contentType () {
    let content = [];
    const current = this.props.current;
    const type = current.type.
                         toString().
                         toLowerCase();

    let path = `/${type}`;
    if (type === 'project') {
      path = `/${type}/${
        current.pro.id
      }`;

      content.push(
        <project.Left pro={
          current.pro
        } key='left'/>
      );

      content.push(
        <project.Right pro={
          current.pro
        } key='right'/>
      );
    } else if (type === 'article') {
      path = `/${type}/${
        current.art.stamp
      }`;

      if (current.art.stamp === 'new') {
        if (!this.props.user.id) {
          this.props.dispatch({
            type: 'UPDATE_CURRENT',
            payload: {
              type: 'login'
            }
          });
          return;
        } content.push(<article.New key='new'/>);
      } else {
        content.push(<article.Left key='left'/>);
        content.push(<article.Right key='right'/>);
      }
    } else if (type === 'blog') {
      content.push(<blog.Top key='top'/>);
      content.push(<blog.Bottom key='bottom'/>);
    } else if (type === 'about') {
      content.push(<about.Left key='left'/>);
      content.push(<about.Right key='right'/>);
    } else if (type === 'login') {
      content.push(<login.Top key='top'/>);
      content.push(<login.Bottom key='bottom'/>);
    } else if (type === 'reset') {
      path = '/login';
      content.push(<login.Top key='top' reset={true}/>);
      content.push(<login.Bottom key='bottom' reset={true}/>);
    } else if (type === 'projects') {
      content.push(<projects.Top key='top'/>);
      content.push(<projects.Bottom key='bottom'/>);
    } else {
      content.push(<home.Left key='left'/>);
      content.push(<home.Right key='right'/>);
    } history.pushState(null, null, path);

    return content;
  }

  render () {
    let classes = 'content',
        type = this.props.current.type;

    switch (type.toString().toLowerCase()) {
      case 'article':
        setTimeout(() => {
          lazyCSS('/css/new.min.css');
        }, 50);
        setTimeout(() => {
          linkAWS('https://anagabriel.s3.us-west-2.amazonaws.com');
        }, 100);
        classes += ' art';
        break;
      case 'about':
      case 'project':
        setTimeout(() => {
          lazyCSS('/css/projects.min.css');
        }, 50);
        classes += ' abt';
        break;
      case 'blog':
        setTimeout(() => {
          linkAWS('https://anagabriel.s3.us-west-2.amazonaws.com');
        }, 100);
      case 'login':
      case 'reset':
      case 'projects':
        setTimeout(() => {
          lazyCSS('/css/projects.min.css');
        }, 50);
        classes += ' prj';
        break;
    }

    return (
      <div className={classes}>{
        this.contentType()
      }</div>
    );
  }

}
