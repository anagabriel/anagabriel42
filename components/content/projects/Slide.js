import React, { Component } from 'react'
import Project from './Project'

export default class Slide extends Component {
  render () {
    const projects = this.props.pros.map(p => {
      if (!p) return '';
      return <Project key={p.id} pro={p} />;
    });

    return <div>{projects}</div>;
  }
}
