import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Top extends Component {

  home (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  render () {
    return (
      <section className='top'>
        <Button shape='circle' onClick={
          e => this.home(e)
        } icon='arrow-left'/>
        <h1>projects</h1>
      </section>
    );
  }

}
