import React, { Component } from 'react'
import { connect } from 'react-redux'

import Slide from './Slide'

import { Carousel } from 'antd'

@connect((store) => {
  return {
    current: store.current,
    projects: store.projects
  };
})

export default class Bottom extends Component {

  slides () {
    const projects = this.props.projects;
    const length = projects.length;

    const sections = Math.trunc(
      length % 3 ? length / 3 + 1 : length / 3
    );

    let s = sections,
        p = length;

    let slides = new Array(s);

    while (s && p) {
      let sub = new Array(3);

      for (let i = 0; i < 3 && p; i++, p--) {
        sub[i] = projects[length - p];
      }

      slides[sections - s] = (
        <Slide key={sections-s} pros={sub} />
      ), s--;
    }

    return slides;
  }

  render () {
    setTimeout(images, 1500);
    let slides = this.slides();

    return (
      <section className='bottom'>
        <Carousel autoplay>{
          slides
        }</Carousel>
      </section>
    );
  }
}
