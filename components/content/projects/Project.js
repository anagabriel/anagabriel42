import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Card, Button, Skeleton } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Project extends Component {

  view (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {
        type: 'project',
        pro: this.props.pro
      }
    });
  }

  render () {
    return (
      <Button onClick={e => this.view(e)}>
        <div className='thumbnail'>
          <img data-src={this.props.pro.img} alt={`an image of ${this.props.pro.title}`}/>
        </div>
        <div className='descr'>
          <h2>{this.props.pro.title}</h2>
          <p>{this.props.pro.descr}</p>
        </div>
      </Button>
    );
  }

}
