import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button, message } from 'antd'

@connect((store) => {
  return {
    current: store.current,
    user: store.user
  };
})

export default class Top extends Component {

  home (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  add (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {
        type: 'article',
        art: {stamp: 'new'}
      }
    });
  }

  render () {
    const admin = this.props.user.id === 'ana.gabriel2012@gmail.com';
    return (
      <section className={admin ? 'top admin' : 'top'}>
        <Button shape='circle' onClick={
          e => this.home(e)
        } icon='arrow-left'/>
        <h1 className={admin ? 'admin' : ''}>blog</h1>
        {
          !admin ? '' :
          <Button shape='circle' onClick={
            e => this.add(e)
          } icon='plus' className='add'/>
        }
      </section>
    );
  }

}
