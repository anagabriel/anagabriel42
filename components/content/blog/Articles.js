import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  List,
  Icon,
  Spin,
  Avatar,
  message
} from 'antd'
import InfiniteScroll from 'react-infinite-scroller'

const IconText = ({ type, text, click, i, clicked }) => (
  <span onClick={e => click(e, i)}>{
    clicked ? <Icon className={
      `${type}`
    } type={type} theme='twoTone' twoToneColor={
      type == 'star-o' ? '#e8bc00' : '#40a9ff'
    } size='large'/> : <Icon className={
      `${type}`
    } type={type} size='large'/>
  }{
    text
  }</span>
);

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Articles extends React.Component {

  state = {
    data: [],
    loading: false,
    hasMore: true,
    length: 0,
    last: null
  }

  componentDidMount = () => {
    this.get(`/arts/count`).then(res => {
      this.setState({
        length: res.result
      });
    });

    this.get(`/arts`).
    then(res => this.sort(res)).
    then(res => this.stars(res)).
    then((res) => {
      let state = {
        data: res.result.data || res.result
      };

      if (res.result.last)
        state.last = res.result.last;
      this.setState(state);
    });
  }

  sort = (res) => {
    let data = res.result.data || res.result;

    data.sort((a, b) => {
      return (0 - a.stamp.localeCompare(b.stamp));
    });

    !!res.result.data ? res.result.data = data : res.result = data;
    return res;
  }

  stars = (res) => {
    if (!res.result.stars) return res;

    const l = res.result.data.length;
    for (let i = 0; i < l; i++) {
      if (res.result.stars.find((e) => {
        return e.art == res.result.data[i].stamp
      })) {
        res.result.data[i].starred = true;
      }
    }

    return res;
  }

  view = (e, art) => {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {
        art,
        type: 'article'
      }
    });
  }

  get = (url) => {
    return fetch(url, {
      method: 'get',
      contentType: 'application/json'
    }).then(res => res.json());
  }

  star = (e, i) => {
    if (!this.props.user.id) {
      message.error(
        `you need to login first`
      );
      return;
    }

    const art = this.state.data[i];
    if (art.starred) return;
    const url = `/arts/star/${art.stamp}`,
          body = JSON.stringify({
            stars: (art.stars || 0) + 1,
            id: this.props.user.id
          });

    fetch(url, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body
    }).then(res => res.json()).
    then((res) => {
      if (!res.success) {
        message.error(
          res.result.message.toString()
        );
        return;
      }

      message.success(
        `you've starred "${art.title}"`
      );

      let data = this.state.data;
      data[i].stars = res.result.stars;
      data[i].starred = true;
      this.setState({data});
    });
  }

  like = (e, i) => {
    const art = this.state.data[i];
    if (art.liked) return;
    const url = `/arts/like/${art.stamp}`,
          body = JSON.stringify({
            likes: (art.likes || 0) + 1
          });

    fetch(url, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body
    }).then(res => res.json()).
    then((res) => {
      if (!res.success) {
        message.error(
          res.result.message.toString()
        );
        return;
      }

      message.success(
        `you've liked "${art.title}"`
      );

      let data = this.state.data;
      data[i].likes = res.result.likes;
      data[i].liked = true;
      this.setState({data});
    });
  }

  handleInfiniteOnLoad = () => {
    let data = this.state.data;

    if (data.length == this.state.length) {
      message.info(
        'all content has been loaded'
      );

      this.setState({
        data,
        hasMore: false,
        loading: false
      });

      return;
    } this.setState({
      loading: true
    });


    let url = '/arts';
    if (this.state.last) url += `/?last=${this.state.last.stamp}`;
    this.get(url).
    then((res) => {
      data = data.concat(
        res.result.data || res.result
      ).sort(
        (a, b) => (0 - a.stamp.localeCompare(b.stamp))
      );

      let state = {
        data,
        loading: false
      };

      if (res.result.last)
        state.last = res.result.last;

      this.setState(state);
    });
  }

  render = () => {
    setTimeout(images, (1500));
    return (
      <div className='infinite-container'>
        <InfiniteScroll
          initialLoad={false}
          pageStart={0}
          loadMore={() => this.handleInfiniteOnLoad()}
          hasMore={!this.state.loading && this.state.hasMore}
          useWindow={false}
        >
          <List
            itemLayout='vertical'
            size='large'
            dataSource={this.state.data}
            renderItem={(item, i) => (
              <List.Item
                key={item.stamp}
                actions={[
                  <IconText type='star-o' text={
                    item.stars
                  } click={
                    (e, i) => this.star(e, i)
                  } i={i} clicked={
                    !!item.starred
                  }/>,
                  <IconText type='like-o' text={
                    item.likes
                  } click={
                    (e, i) => this.like(e, i)
                  } i={i} clicked={
                    !!item.liked
                  }/>
                ]} extra={
                  <a onClick={
                    e => this.view(e, item)
                  }><img className='unset' width={272} alt={
                    `image of ${item.title}`
                  } data-src={item.img1 || '/img/screen.jpg'} /></a>
                }
              >
                <List.Item.Meta
                  avatar={<Avatar src='favicon.png' className='av'/>}
                  title={<a onClick={
                    e => this.view(e, item)
                  }>{item.title}</a>}
                  description={
                    `${item.author} - ${new Date(item.stamp).toDateString()}`
                  }
                />
                <div>{item.descr}</div>
              </List.Item>
            )}
          >
            {this.state.loading && this.state.hasMore && (
              <div className='loading-container'>
                <Spin />
              </div>
            )}
          </List>
        </InfiniteScroll>
      </div>
    );
  }
}
