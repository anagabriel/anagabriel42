import React, { Component } from 'react'
import Articles from './Articles'

export default class Bottom extends Component {
  render () {
    return (
      <section className='bottom'>
        <Articles />
      </section>
    );
  }
}
