import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Left extends Component {

  back (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: 'projects'}
    });
  }

  render () {
    setTimeout(images, 1500);
    return (
      <aside className='left about project'>
        <div className='top'>
          <Button shape='circle' icon='arrow-left' onClick={
            e => this.back(e)
          }/>
          <div className='preview'>
            <img data-src={this.props.pro.img} alt={`an image of ${this.props.pro.title}`}/>
          </div>
        </div>
      </aside>
    );
  }

}
