import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Right extends Component {

  back (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: 'projects'}
    });
  }

  render () {
    return (
      <aside className='right about project'>
        <div className='top'>
          <Button shape='circle' onClick={
            e => this.back(e)
          } icon='arrow-left'/>
          <ul>
            <li>{this.props.pro.title}</li>
            <li>{this.props.pro.used}</li>
          </ul>
        </div>
        <div className='bottom'>
          <Button type='default' href={
            this.props.pro.code
          } target='_blank' icon='code'/>
          <Button type='default' href={
            this.props.pro.url
          } target='_blank' icon='link'/>
        </div>
      </aside>
    );
  }

}
