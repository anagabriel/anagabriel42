import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class Right extends Component {

  componentDidMount () {
    setTimeout(images, (1000));
  }

  render () {
    const art = this.props.current.art;

    let list = [];
    if (art.img1) list.push(
      <div key='img1' className='img1'>
        <img data-src={art.img1} alt={
          `image of ${art.title}`
        }/>
      </div>
    );

    if (art.img2) list.push(
      <div key='img2' className='img2'>{
        art.img2.indexOf('.gif') === -1 ?
        <img data-src={art.img2} alt={
          `image of ${art.title}`
        }/> :
        <img data-src={art.img2} style={
          { marginTop: 'calc(-50vh)' }
        } alt={
          `image of ${art.title}`
        }/>
      }</div>
    );

    if (art.img3) list.push(
      <div key='img3' className='img3'>
        <img data-src={art.img3} alt={
          `image of ${art.title}`
        }/>
      </div>
    );

    if (!list.length) list.push(
      <div key='img1' className='img1'>
        <img data-src='/img/screen.jpg' alt={
          `image of ${art.title}`
        }/>
      </div>
    );

    return (
      <aside className='right art'>{
        list
      }</aside>
    );
  }
}
