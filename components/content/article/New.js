import React, { Component } from 'react'
import Article from './Article'

import { Form } from 'antd'

export default class New extends Component {

  render () {
    const Wrapped = Form.create({
      name: 'normal_login'
    })(Article);

    return (
      <section className='article'>
        <Wrapped />
      </section>
    );
  }

}
