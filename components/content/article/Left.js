import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button, message } from 'antd'

const parseLinks = require(
  '../../../utils/parse'
).parseLinks;

@connect((store) => {
  return {
    current: store.current,
    user: store.user
  };
})

export default class Left extends Component {

  componentDidMount () {
    fetch(`/arts/${this.props.current.art.stamp}`).
    then(res => res.json()).
    then(res => {
      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: {
          type: 'article',
          art: res.result
        }
      });
    });
  }

  back (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: 'blog'}
    });
  }

  lines (text) {
    return text.split(/(?:\r\n|\r|\n)/g).map(
      (e, i) => {
        let elist = parseLinks(e, (name, href) => {
          return <a href={href} target='_blank'>{
              name
          }</a>;
        });

        return <p key={`p-${i}`}>{
          elist.length ? elist : e
        }</p>;
      }
    );
  }

  delete () {
    const art = this.props.current.art;
    const res = confirm(
      `Are you sure you want to delete "${art.title}"?`
    );

    if (!res) return;

    fetch(`/arts/${art.stamp}`, {
      method: 'delete'
    }).then(res => res.json()).
    then(res => {
      if (!res.success) {
        message.error(
          res.result.message.toString()
        );
        return;
      }

      message.success(
        `${art.title} was deleted`
      );

      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: { type: 'blog' }
      });
    });
  }

  edit () {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {
        type: 'article',
        art: {stamp: 'new'},
        old: this.props.current.art
      }
    });
  }

  render () {
    const art = this.props.current.art,
          text = this.lines(art.text || ''),
          admin = this.props.user.id == 'ana.gabriel2012@gmail.com' ? (
                    <div className='wrap-buttons'>
                      <Button shape='circle' onClick={
                        e => this.delete()
                      } type='danger' icon='delete'/>
                      <Button shape='circle' onClick={
                        e => this.edit()
                      } type='primary' icon='edit'/>
                    </div>
                  ) : '';

    return (
      <aside className='left about art'>
        <div className='top'>
          <Button shape='circle' onClick={
            e => this.back(e)
          } icon='arrow-left'/>
        </div>
        <div className='article'>
          <h1>{art.title}</h1>
          <h3>{
            `${art.author} - ${new Date(art.stamp).toDateString()}`
          }</h3>
          {text}
          {admin}
        </div>
      </aside>
    );
  }

}
