import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  Form,
  Icon,
  Input,
  Button,
  Upload,
  message
} from 'antd'

const { TextArea } = Input;

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Article extends Component {

  state = {
    fileList: []
  }

  componentDidMount () {
    if (!this.props.current.old) return;

    const n = 'normal_login_',
          old = this.props.current.old;

    let text = document.getElementById(n + 'text'),
        title = document.getElementById(n + 'title'),
        descr = document.getElementById(n + 'descr');

    text.value = old.text;
    title.value = old.title;
    descr.value = old.descr;
  }

  handleChange (info) {
    let fileList = info.fileList;
    fileList = fileList.slice(-3);

    fileList = fileList.map((file) => {
      if (file.response) {
        file.url = file.response.result.url;
      } return file;
    });

    fileList = fileList.filter((file) => {
      return file.response ?
             file.response.success : false;
    });

    this.setState({ fileList });
  }

  normFile (e) {
    return Array.isArray(e) ? e : e && e.fileList;
  }

  handleSubmit (e) {
    e.preventDefault();

    const cur = this.props.current;
    this.props.form.validateFields((err, values) => {
      if (err && !cur.old) return;

      if (!cur.old) {
        values.author = this.props.user.id;
        for (let i=0; i < this.state.fileList.length; i++) {
          values[`img${i+1}`] = this.state.fileList[i].url;
        } delete values.upload;
      }

      const opt = {
        method: !cur.old ? 'post' : 'put',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }, body: JSON.stringify(values)
      };

      const url = !cur.old ? '/arts' : `/arts/${cur.old.stamp}`;
      fetch(url, opt).
      then(res => res.json()).
      then(res => {
        if (!res.success) {
          message.error(
            res.result.message.toString()
          );
          return;
        }

        const name = values.title || cur.old.title,
              end = !cur.old ? 'created' : 'updated';

        message.success(
          `"${name}" was ${end}`
        );

        this.props.dispatch({
          type: 'UPDATE_CURRENT',
          payload: {type: 'blog'}
        });
      });
    });
  }

  cancel (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: 'blog'}
    });
  }

  render () {

    const { getFieldDecorator } = this.props.form,
          props = {
            action: '/arts/image',
            onChange: this.handleChange.bind(this),
            multiple: true
          };

    return (
      <Form onSubmit={
        e => this.handleSubmit(e)
      } className='new'>

        <Form.Item>
          {getFieldDecorator('title', {
            rules: [{
              required: true,
              message: 'Please, add a title!'
            }]
          })(
            <Input type='text' placeholder='title' />
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('descr', {
            rules: [{
              required: true,
              message: 'Please, add a description!'
            }]
          })(
            <TextArea placeholder='description' className='description'/>
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('upload', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile.bind(this),
          })(
            <Upload {...props} fileList={this.state.fileList}>
              <Button>
                <Icon type='upload' /> Upload
              </Button>
            </Upload>
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('text', {
            rules: [{
              required: true,
              message: 'Please, add content!'
            }]
          })(
            <TextArea placeholder='content' className='textarea'/>
          )}
        </Form.Item>

        <div className='wrap-buttons'>
          <Form.Item className='buttons'>
            <Button shape='circle' type='danger' onClick={
              e => this.cancel(e)
            } icon='close' className='cancel'/>
          </Form.Item>

          <Form.Item className='buttons'>
            <Button shape='circle' type='primary' htmlType={
              'submit'
            } icon='save' className='save'/>
          </Form.Item>
        </div>

      </Form>
    );
  }

}
