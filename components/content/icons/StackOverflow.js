import React, { Component } from 'react'
import { Icon } from 'antd'

const StackOverflowSvg = () => (
  <object data='img/stackoverflow.svg' type='image/svg+xml'>
    <img data-src='img/stackoverflow.png'/>
  </object>
);

export default class StackOverflow extends Component {
  render () {
    return <Icon component={StackOverflowSvg} {...this.props} />
  }
}
