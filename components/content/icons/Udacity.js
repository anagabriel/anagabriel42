import React, { Component } from 'react'
import { Icon } from 'antd'

const UdacitySvg = () => (
  <object data='img/udacity.svg' type='image/svg+xml'>
    <img data-src='img/udacity.png'/>
  </object>
);

export default class Udacity extends Component {
  render () {
    return <Icon component={UdacitySvg} {...this.props} />
  }
}
