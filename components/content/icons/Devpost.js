import React, { Component } from 'react'
import { Icon } from 'antd'

const DevpostSvg = () => (
  <object data='img/devpost.svg' type='image/svg+xml'>
    <img data-src='img/devpost.png'/>
  </object>
);

export default class Devpost extends Component {
  render () {
    return <Icon component={DevpostSvg} {...this.props} />
  }
}
