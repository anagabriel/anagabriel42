import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  Form,
  Icon,
  Input,
  Button,
  message
} from 'antd'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class SignUp extends Component {

  handleSubmit (e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) return;

      fetch('/users', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }, body: JSON.stringify(values)
      }).then(res => res.json())
      .then(res => {

        if (!res.success) {
          message.error(res.result.message.toString());
          return;
        }

        message.success(
          `${values.email}'s account has been created`
        );

        if (browser().indexOf('edge') !== -1) {
          document.cookie += ` id=${
            res.result.id
          } login=${res.result.login}`;
        }

        this.props.dispatch({
          type: 'UPDATE_USER',
          payload: res.result
        });

        this.props.dispatch({
          type: 'UPDATE_CURRENT',
          payload: {type: ''}
        });

      });
    });
  }

  render () {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={
        e => this.handleSubmit(e)
      } className='login-form'>
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [{
              required: true,
              message: 'Please, input your email!'
            }]
          })(
            <Input type='email' suffix={
              <Icon type='user' style={{
                color: 'rgba(0,0,0,.25)'
              }}/>
            } placeholder='email' />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{
              required: true,
              message: 'Please, input your password!'
            }]
          })(
            <Input type='password' suffix={
              <Icon type='lock' style={{
                color: 'rgba(0,0,0,.25)'
              }}/>
            } placeholder='password'/>
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('confirm', {
            rules: [{
              required: true,
              message: 'Please, confirm your password!'
            }]
          })(
            <Input type='password' suffix={
              <Icon type='lock' style={{
                color: 'rgba(0,0,0,.25)'
              }}/>
            } placeholder='confirm password' />
          )}
        </Form.Item>
        <Form.Item>
          <Button type='primary' htmlType='submit' className='login-form-button'>
            submit
          </Button>
        </Form.Item>
      </Form>
    );
  }

}
