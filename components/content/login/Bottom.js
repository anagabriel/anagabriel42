import React, { Component } from 'react'
import Fields from './Fields'

export default class Bottom extends Component {
  render () {
    return (
      <section className='bottom login'>
        <Fields reset={this.props.reset}/>
      </section>
    );
  }
}
