import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button } from 'antd'
import 'antd/dist/antd.css'

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Top extends Component {

  componentDidMount () {
    if (!this.props.user.id || this.props.reset) return;
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  componentDidUpdate () {
    if (!this.props.user.id || this.props.reset) return;
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  home (e) {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {type: ''}
    });
  }

  render () {
    return (
      <section className='top login'>
        <Button shape='circle' onClick={
          e => this.home(e)
        } icon='arrow-left'/>
      </section>
    );
  }

}
