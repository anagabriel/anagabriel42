import React, { Component } from 'react'
import { Form, Button } from 'antd'

import Reset from './Reset'
import SignIn from './SignIn'
import SignUp from './SignUp'

export default class Fields extends Component {

  state = {
    signin: true
  }

  update (e, signin) {
    if (this.props.reset) return;
    this.setState({
      signin
    });
  }

  render () {
    const Wrapped = Form.create({
      name: 'normal_login'
    })(
      this.props.reset ? Reset :
      this.state.signin ? SignIn : SignUp
    );

    return (
      <div className='login'>
        <div className='tabs'>
          <a className={
            this.state.signin ? 'active' : 'not-active'
          } onClick={
            e => this.update(e, true)
          }>sign in</a>
          <a className={
            !this.state.signin ? 'active' : 'not-active'
          } onClick={
            e => this.update(e, false)
          }>sign up</a>
        </div>
        <Wrapped />
      </div>
    );
  }

}
