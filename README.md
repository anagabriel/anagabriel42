# anagabriel42
a repo for my personal website ^.^

## Mockups
### Home
![home page](.readme/anagabriel.png)
### About
![about](.readme/about.png)
### Projects
![projects](.readme/projects.png)
### Project
![project](.readme/project.png)
### Blog
![blog](.readme/blog.png)
### Article
![article](.readme/blog_post.png)
### Login
![login](.readme/login.png)
### Dashboard
![dashboard](.readme/dashboard.png)

## Audits
### Home
![home page](.readme/audit/home.png)
### About
![about](.readme/audit/about.png)
### Projects
![projects](.readme/audit/projects.png)
### Project
![project](.readme/audit/project.png)
### Blog
![blog](.readme/audit/blog.png)
### Article
![article](.readme/audit/article.png)

## Coding hints

### BST
Notice how simple finding the second largest node got when we divided it into two cases:

1. The largest node has a left subtree.
2. The largest node does not have a left subtree

### Stacks
Two common uses for stacks are:

1. parsing (like in this problem)
1. tree or graph traversal (like depth-first traversal)
