const table = require('../utils/table');
const name = table.set('me-arts', 'stamp', 10).TableName;

module.exports = {

  table: (callback) => {
    table.table(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  count: (callback) => {
    table.count(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback, last) => {
    table.all(name, (err, data, l) => {
      if (l) data = {
        data,
        last: l
      }; callback(err, data);
      return;
    }, last);
  },

  get: (id, callback) => {
    table.get(name, id, (err, data) => {
      callback(err, data);
      return;
    });
  },

  find: (params, callback, last) => {
    table.find(name, params, (err, data, l) => {
      if (l) data = {
        data,
        last: l
      }; callback(err, data);
      return;
    }, last);
  },

  create: (params, callback) => {
    table.get(name, params.stamp, (err, data) => {
      if (data) {
        callback({
          message: 'article already exists!'
        }, null);
        return;
      } table.create(name, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  inc: (id, prop, callback) => {
    let params = {};
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'article does not exist!'
        }, null);
        return;
      } params[prop] = parseInt(`${data[prop] || 0}`) + 1;
      table.update(name, id, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'article does not exist!'
        }, null);
        return;
      } table.update(name, id, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  delete: (id, callback) => {
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'article does not exist!'
        }, null);
        return;
      } table.delete(name, id, (e, d) => {
        callback(e, data);
        return;
      });
    });
  }

}
