const path = document.location.pathname.toString().toLowerCase();

let cur = {};
cur.type = path.substring(1);

if (cur.type.indexOf('/') !== -1) {
  const tmp = cur.type.substring(
    0, cur.type.indexOf('/')
  );

  const i = cur.type.substring(
    cur.type.indexOf('/') + 1
  );

  cur.type = tmp;
  if (tmp === 'project')
    cur.pro = {id: i};
  else if (tmp === 'article')
    cur.art = {stamp: i};
}

export default function currentReducer(state=cur, action) {
  switch (action.type) {
    case 'UPDATE_CURRENT':
      state = action.payload
      break;
  } return state;
};
