import { combineReducers, createStore } from 'redux'

import user from './user'
import current from './current'
import projects from './projects'

export default createStore(
  combineReducers({
    user, current, projects
  })
)
