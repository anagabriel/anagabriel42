export default function projectsReducer(state=[], action) {
  switch (action.type) {
    case 'UPDATE_PROJECTS':
      state = action.payload;
      state.sort((a, b) => {

        const ak = a.id.length,
              bk = b.id.length;

        if (ak === bk) {
          return a.id.localeCompare(b.id);
        } return ak - bk;

      });
      break;
  } return state;
};
