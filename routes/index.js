const express = require('express');
const router = express.Router();

const std = require('../utils/std');
const arts = require('../controllers/arts');
const tabs = require('../controllers/tabs');

const title = 'ana-gabriel';
const description = `
  This Ana-gabriel\'s personal website. 
  Pizza lover, and of component-based web design.
  She graduated from Purdue University with a degree 
  in Computer Science, and is currently a Software Engineer.
`;

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/blog', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/about', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/login', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/article/new', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/article/:stamp', (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  arts.get(req.params.stamp, (err, data) => {
    if (err || !data)
      res.render('error', {
        error: {status: 404},
        message: 'Not Found'
      });
    else
      res.render('index', {
        title, description, ...std.grabURL(req)
      });
  });
});

router.get('/project/:id', (req, res, next) => {
  tabs.get(req.params.id, (err, data) => {
    if (err || !data)
      res.render('error', {
        error: {status: 404},
        message: 'Not Found'
      });
    else
      res.render('index', {
        title, description, ...std.grabURL(req)
      });
  });
});

router.get('/projects', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

module.exports = router;
