const express = require('express');
const router = express.Router();

const arts = require('../controllers/arts');
const text = require('../controllers/text');
const notes = require('../controllers/notes');

const std = require('../utils/std');
const val = require('../utils/val');

const s3 = require('../utils/S3');

const multer = require('multer');
const multerS3 = require('multer-s3');

const bucket = 'anagabriel';

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: bucket,
    acl: 'public-read',
    metadata: (req, file, cb) => {
      cb(null, {fieldName: file.fieldname});
    },
    key: (req, file, cb) => {
      cb(null, Date.now().toString() + '-' + file.originalname)
    }
  })
});

/* GET all arts */
router.get('/', (req, res, next) => {
  const last = req.query.last ? {
    stamp: req.query.last.toUpperCase()
  } : null;

  arts.all((err, data) => {
    if (!!req.cookies['id']) {
      const note = {
        email: req.cookies['id']
      }; notes.find(note, (e, d) => {
        if (d.length) {
          res.json({
            success: true,
            result: !!data.last ? {
              data: data.data,
              last: data.last,
              stars: d
            } : {
              data,
              stars: d
            }
          });
        } else res.json(
          std.response(err, data)
        );
      });
    } else res.json(
      std.response(err, data)
    );
  }, last);
});

/* GET total articles */
router.get('/count', (req, res, next) => {
  arts.count((err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* CREATE an article */
router.post('/', val.admin, (req, res, next) => {
  req.body.stamp = new Date().toISOString();

  let txt = {
    stamp: req.body.stamp,
    text: req.body.text
  };

  delete req.body.text;
  arts.create(req.body, (err, data) => {
    text.create(txt, (e, d) => {
      res.json(
        std.response(err, data)
      );
    });
  });
});

/* CREATE an image for an article */
router.post('/image', val.admin, upload.single('file'), (req, res) => {
  res.json(
    std.response(null, {
      url: req.file.location,
      name: req.file.key
    })
  );
});

/* GET an article */
router.get('/:stamp', (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  arts.get(req.params.stamp, (err, data) => {
    text.get(req.params.stamp, (e, d) => {
      if (d) data.text = d.text;
      res.json(
        std.response(err, data)
      );
    });
  });
});

/* UPDATE to like an article */
router.put('/like/:stamp', (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  arts.inc(req.params.stamp, 'likes', (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* UPDATE to star an article */
router.put('/star/:stamp', val.user, (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  arts.inc(req.params.stamp, 'stars', (err, data) => {
    const note = {
      stamp: new Date().toISOString(),
      email: req.cookies['id'],
      art: req.params.stamp
    }; if (!err) {
      notes.create(note, (e, d) => {
        res.json(
          std.response(err, data)
        );
        return;
      });
      return;
    } res.json(
      std.response(err, data)
    );
  });
});

/* UPDATE an article */
router.put('/:stamp', val.admin, (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  if (!req.body.text)
    arts.update(req.params.stamp, req.body, (err, data) => {
      res.json(
        std.response(err, data)
      );
    });
  else
    text.update(req.params.stamp, req.body, (err, data) => {
      res.json(
        std.response(err, data)
      );
    });
});

/* REMOVE an article */
router.delete('/:stamp', val.admin, (req, res, next) => {
  req.params.stamp = req.params.stamp.toUpperCase();
  arts.delete(req.params.stamp, (err, data) => {

    if (data.img1) {
      const img1 = data.img1.split('/');
      const params = {
        Bucket: bucket,
        Key: img1[img1.length-1]
      };
      s3.deleteObject(params, (e, d) => {
        console.log(e || d);
        return;
      });
    }

    if (data.img2) {
      const img2 = data.img2.split('/');
      const params = {
        Bucket: bucket,
        Key: img2[img2.length-1]
      };
      s3.deleteObject(params, (e, d) => {
        console.log(e || d);
        return;
      });
    }

    if (data.img3) {
      const img3 = data.img3.split('/');
      const params = {
        Bucket: bucket,
        Key: img3[img3.length-1]
      };
      s3.deleteObject(params, (e, d) => {
        console.log(e || d);
        return;
      });
    }

    text.delete(req.params.stamp, (e, d) => {
      res.json(
        std.response(err || e, data || d)
      );
    });
  });
});

module.exports = router;
