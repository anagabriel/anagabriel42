const express = require('express');
const router = express.Router();

const users = require('../controllers/users');

const std = require('../utils/std');
const val = require('../utils/val');
const email = require('../utils/email');

const domain = 'anagabriel.me';

/* GET reset session */
router.get('/:id', (req, res, next) => {
  const crypto = require('crypto');
  const code = std.string(6);
  crypto.pbkdf2(code, 'salt', 100000, 64, 'sha512',
    (err, derivedKey) => {
      if (err) {
        res.json({
          success: false,
          result: err
        });
        return;
      }

      const user = {
        reset: derivedKey.toString('hex'),
        stamp: new Date().toISOString()
      };

      users.update(req.params.id, user, (e, d) => {
        if (!e) email.resetPassword(req.params.id, code);
        if (d.reset) delete d.reset;
        if (d.password) delete d.password;
        if (d.login) delete d.login;
        console.log(e, d);
        res.json(
          std.response(e, d)
        );
      });
    }
  );
});

/* POST user's new password */
router.post('/', (req, res, next) => {
  const result = std.isUserValid(req.body);
  if (!result.success) {
    res.json({
      success: false,
      result: result
    });
    return;
  }

  const id = result.user.id;
  if (req.body.email) delete req.body.email;

  const login = std.string(32);
  const crypto = require('crypto');
  const reset = crypto.pbkdf2Sync(
    req.body.code,
    'salt',
    100000,
    64,
    'sha512'
  ).toString('hex');
  const password = crypto.pbkdf2Sync(
    req.body.password,
    'salt',
    100000,
    64,
    'sha512'
  ).toString('hex');

  users.reset(id, {
    password,
    reset,
    login
  }, (err, data) => {

    res.cookie('id', id, {domain});
    res.cookie('login', login, {domain});

    res.json(
      std.response(err, data)
    );
  });
});

module.exports = router;
