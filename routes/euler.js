const express = require('express');
const router = express.Router();

const euler = require('../controllers/euler');
const std = require('../utils/std');

const title = 'project euler | ana-gabriel';
const description = 'Ana-Gabriel\'s Project Euler. Over 700 puzzles to solve.';

router.get('/', (req, res, next) => {
  res.render('euler', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/:id', (req, res, next) => {
  euler.get(req.params.id, (err, data) => {
    if (err || !data)
      res.render('error', {
        error: {status: 404},
        message: 'Not Found'
      });
    else
      res.render('euler', {
        title, description, ...std.grabURL(req)
      });
  });
});

module.exports = router;
