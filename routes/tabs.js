const express = require('express');
const router = express.Router();

const std = require('../utils/std');
const tabs = require('../controllers/tabs');

const val = require('../utils/val');

/* GET all tabs */
router.get('/', (req, res, next) => {
  tabs.all((err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* CREATE a tab */
router.post('/', val.admin, (req, res, next) => {
  req.body.id = std.string(7);
  console.log(req.body);
  tabs.create(req.body, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* GET a tab */
router.get('/:id', (req, res, next) => {
  tabs.get(req.params.id, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* UPDATE a tab */
router.put('/:id', val.admin, (req, res, next) => {
  tabs.update(req.params.id, req.body, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* REMOVE a tab */
router.delete('/:id', val.admin, (req, res, next) => {
  tabs.delete(req.params.id, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

module.exports = router;
