const express = require('express');
const router = express.Router();

const users = require('../controllers/users');

const std = require('../utils/std');
const val = require('../utils/val');

const domain = 'anagabriel.me';

/* GET all users */
router.get('/', (req, res, next) => {
  users.all((err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* POST create a user */
router.post('/', (req, res, next) => {

  const result = std.isUserValid(req.body);
  if (!result.success) {
    res.json({
      success: false,
      result: result
    });
    return;
  }

  let user = result.user;
  const crypto = require('crypto');
  crypto.pbkdf2(user.password, 'salt', 100000, 64, 'sha512',
    (err, derivedKey) => {

      if (err) {
        res.json({
          success: false,
          result: err
        });
        return;
      }

      user.password = derivedKey.toString('hex');
      user.stamp = new Date().toISOString();
      user.login = std.string(32);

      users.create(user, (e, d) => {

        res.cookie('id', user.id, {domain});
        res.cookie('login', user.login, {domain});

        res.json(
          std.response(e, d)
        );
        return;
      });
      return;
    }
  );
});

/* GET a user */
router.get('/:id', (req, res, next) => {
  users.get(req.params.id, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* POST login as a user */
router.post('/:id', (req, res, next) => {
  const crypto = require('crypto');
  crypto.pbkdf2(req.body.password, 'salt', 100000, 64, 'sha512',
    (err, derivedKey) => {

      if (err) {
        res.json({
          success: false,
          result: err
        });
        return;
      }

      const password = derivedKey.toString('hex');
      users.verify(req.params.id, {password}, (err, data) => {

        if (err) {
          res.json(
            std.response(err, data)
          );
          return;
        }

        const user = {
          login: std.string(32),
          stamp: new Date().toISOString()
        }

        users.update(req.params.id, user, (e, d) => {

          res.cookie('id', req.params.id, {domain});
          res.cookie('login', user.login, {domain});

          res.json(
            std.response(e, d)
          );
        });
        return;

      });
      return;
    }
  );
});

/* PUT update a user */
router.put('/:id', val.user, (req, res, next) => {
  req.body.stamp = new Date().toISOString();
  if (req.body.login) delete req.body.login;
  if (req.body.password) delete req.body.password;
  users.update(req.params.id, req.body, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* DELETE a user */
router.delete('/:id', val.user, (req, res, next) => {
  users.delete(req.params.id, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

module.exports = router;
