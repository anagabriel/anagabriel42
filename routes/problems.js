const express = require('express');
const router = express.Router();

const std = require('../utils/std');
const val = require('../utils/val');
const euler = require('../controllers/euler');

const title = 'project euler';
const description = 'ana-gabriel\'s project euler';

router.get('/', (req, res, next) => {
    const last = req.query.last ? {
        id: req.query.last,
    } : null;
    euler.all((err, data) => {
        if (data.length) 
            res.json({
                success: true,
                result: !!data.last ? {
                    data: data.data,
                    last: data.last,
                } : { data },
            });
        else res.json(
            std.response(err, data)
        );
    }, last);
});

router.get('/:id', (req, res, next) => {
  euler.get(req.params.id, (err, data) => {
    data = {data: [data]};
    res.json(
        std.response(err, data)
    );
  });
});

router.post('/:id',  val.admin, (req, res, next) => {
    euler.update(req.params.id, { 
        submit: req.body.submit, 
    }, (err, data) => {
        res.json(
            std.response(err, data)
        );
    });
});

module.exports = router;
