const express = require('express');
const router = express.Router();

const users = require('../controllers/users');

const std = require('../utils/std');
const val = require('../utils/val');

const domain = 'anagabriel.me';

/* GET check session */
router.get('/', (req, res, next) => {
  users.check(req.cookies['id'], {
    login: req.cookies['login']
  }, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* GET check a user's session */
router.get('/:id/:login', (req, res, next) => {
  users.check(req.params.id, {
    login: req.params.login
  }, (err, data) => {
    res.json(
      std.response(err, data)
    );
  });
});

/* DELETE user's a session */
router.delete('/:id/:login', (req, res, next) => {
  const u = {
    id: req.params.id,
    login: req.params.login
  };
  users.check(u.id, u, (err, data) => {
    if (err) res.json(
      std.response(err)
    );
    else users.remove(u, (err, data) => {
      res.cookie('id', '', {domain});
      res.cookie('login', '', {domain});
      res.json(
        std.response(err, data)
      );
    });
  });
});

module.exports = router;
